<?php

$matches = [];

// Excepcion para las  url principal sea index.html
if (in_array( $_SERVER["REQUEST_URI"], [ '/index.html', '/', '' ] )) {
    echo file_get_contents( '/Users/Usuario/Desktop/Programación web/Platzi/API REST/index.html' );
    die;
}

/* la función preg_match hace coger todo el texto raro (corresponde a una expresión regular) para verificar si la url que recibimos
coincide con ese patrón, en este caso básicamente es dice un string que empieza con / tiene cualquier cosa que no sea una barra
después otra / otra vez tiene cualquier cosa que no sea una barra.

Suponiendo que haya habido match, genero yo mismo las variables $_GET utilizando las coincidencias.

Y por último, delego el control en el 'server.php' para que continúe con la llamado como si se hubiese hecho pasándole los parámetros
por la url como hemos hecho hasta ahora
 */
if (preg_match('/\/([^\/]+)\/([^\/]+)/', $_SERVER["REQUEST_URI"], $matches)) {
    $_GET['resource_type'] = $matches[1];
    $_GET['resource_id'] = $matches[2];
    error_log(print_r($matches, 1));
    require 'server.php';
} elseif (preg_match('/\/([^\/]+)\/?/', $_SERVER["REQUEST_URI"], $matches)) {
    /* En este segundo caso hace lo mismo que el primero pero cuando es para una colección y no un recurso particular */
    $_GET['resource_type'] = $matches[1];
    error_log(print_r($matches, 1));
    require 'server.php';
} else {
    // si no coincide con ninguna de las dos anteriores el servidor responderá con un 404 
    error_log('No matches');
    http_response_code(404);
}

/* Si quiero comprobar lo que digo hago lo siguiente:
1.- en la terminal escribo 'php -S localhost:8000 router.php'
2.- abrimos una segunda terminal y escribimos 'curl http://localhost:8000/books'
2.1.- si quires lo mismo de arriba pero un libro en particular 'curl http://localhost:8000/books/1' por ejemplo

 */